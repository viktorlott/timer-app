//
//  clockViewController.swift
//  Tabbs
//
//  Created by Viktor Lott on 9/30/18.
//  Copyright © 2018 Viktor Lott. All rights reserved.
//

import UIKit

class clockViewController: UIViewController {
    
    var name = String()
    var time = Int()
    var timeToString = String()
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = name
        timeLabel.text = timeToString
        
        

        // Do any additional setup after loading the view.
    }
    

}
