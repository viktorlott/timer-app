//
//  FirstViewController.swift
//  Tabbs
//
//  Created by Viktor Lott on 9/17/18.
//  Copyright © 2018 Viktor Lott. All rights reserved.
//

import UIKit




struct Task {
    let name: String
    let time: Int
    var timeToString: String {
        get {
            let sek = Double(self.time)
            let fullmin = sek / 60.0
            let min = fullmin.rounded(.towardZero)
            let remainderSek = sek - (min * 60)
            if min > 60 {
                let hour = min / 60
                let remainderMin = min - (hour.rounded(.towardZero) * 60)
                let timeT: String = "\(Int(hour))h \(Int(remainderMin))min \(Int(remainderSek))sek"
                return timeT
            }
                let timeText: String = "\(Int(min))min \(Int(remainderSek))sek"
                return timeText

        }
    }
}

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var data = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = [Task(name: "Cykla", time: 3042),
                Task(name: "Plugga", time: 10234),
                Task(name: "Vila", time: 52340),
                Task(name: "Short", time: 13)]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("MyTableCell", owner: self, options: nil)?.first as! MyTableCell
        cell.name.text = data[indexPath.row].name
        cell.time.text = data[indexPath.row].timeToString
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "clockViewController") as? clockViewController
        vc?.name = data[indexPath.row].name
        vc?.time = data[indexPath.row].time
        vc?.timeToString = data[indexPath.row].timeToString
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        
        
        // Pass the selected object to the new view controller.
    }
    
}


